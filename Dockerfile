FROM notspecial/amivtex

# Ensure the de_CH.utf-8 locale exists, needed for weekday mapping
RUN apt-get update && apt-get install -y locales && \
    echo "de_CH.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen && \
    rm -rf /var/lib/apt/lists/*

# Create user with home directory and no password and change workdir
RUN useradd -md /contractor contractor
WORKDIR /contractor
# Run on port 8080 (does not require priviledge to bind)
EXPOSE 8080
# Environment variable for config
ENV CONTRACTOR_CONFIG=/contractor/config.py

# Install bjoern and dependencies for install and ghostscript for compression
RUN apt-get update && apt-get install -y \
        musl-dev python-dev gcc libev-dev ghostscript && \
    pip install bjoern

# Install requirements
COPY ./requirements.txt /contractor/requirements.txt
RUN pip install -r /contractor/requirements.txt

# Copy other files
COPY ./ /contractor

# Switch user
USER contractor

# Modified entrypoint to load font url from app config
ENTRYPOINT ["/contractor/contractor_entrypoint.sh"]

# Start bjoern
CMD ["python3", "server.py"]
