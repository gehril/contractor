# -*- coding: utf-8 -*-

"""Provide login via AMIV API."""

from secrets import token_urlsafe
from urllib.parse import urlencode
from functools import wraps
import requests
from requests.compat import urljoin

from flask import redirect, session, render_template, current_app, request, g


TOKEN = 'oauth_token'
STATE = 'oauth_state'


def protected(func):
    """Decorator to require auth."""
    @wraps(func)
    def _protected_view(*args, **kwargs):
        try:
            if _get_session():
                return func(*args, **kwargs)
        except RuntimeError as error:
            # There is a problem with the token!
            return _goodbye(str(error))
        # No Token -> Redirect to Login
        return _redirect_to_oauth()
    return _protected_view


def logout(message):
    """Delete the token in the API."""
    token = session.get(TOKEN, None)

    if token is not None:
        url = urljoin(current_app.config['AMIVAPI_URL'], 'sessions/')
        headers = {'Authorization': token}

        # Check if token even exists, might be deleted already
        response = requests.get(url + token, headers=headers)
        status = response.status_code

        if status != 401:
            if status != 200:
                raise RuntimeError("GET token '%s' returned status-code "
                                   "%s instead of 200" % (token, status))

            data = response.json()
            headers['If-Match'] = data['_etag']
            response = requests.delete(url + data['_id'], headers=headers)

            status = response.status_code
            if status != 204:
                raise RuntimeError("DELETE token '%s' returned status-code "
                                   "%s instead of 200" % (token, status))

    return _goodbye(message)


def _redirect_to_oauth():
    """Redirect to the AMIV API OAuth page."""
    url = urljoin(current_app.config['AMIVAPI_URL'], 'oauth')

    # Delete existing token (if any) and create new, random state
    session.pop(TOKEN, None)
    session[STATE] = token_urlsafe(16)

    query = urlencode({
        'response_type': 'token',
        'redirect_uri': current_app.config['OAUTH_URI'],
        'client_id': current_app.config['OAUTH_ID'],
        'state': session[STATE],
    })

    return redirect('%s?%s' % (url, query))


def _goodbye(message):
    """Delete session data and show goodbye (resp. error) page."""
    session.pop(TOKEN, None)
    session.pop(STATE, None)
    return render_template('logout.html', message=message)


def _get_session():
    """Return True if logged in, False otherwise.

    Token will be taken from URL params (if state matches) or session.
    g.username will be set to full name of user.

    Since the user can just modify the URL to provide any token,
    it needs to be verified with AMIV API.
    """
    url = urljoin(current_app.config['AMIVAPI_URL'], 'sessions')
    token = request.args.get('access_token')
    state = request.args.get('state')

    if token and state and (state == session.get(STATE)):
        session[TOKEN] = token
    else:
        token = session.get(TOKEN)

    # Verify token with API (and get user name while at it)
    if token:
        headers = {'Authorization': token}
        params = {
            'where': '{"token": "%s"}' % token, 'embedded': '{"user": 1}'
        }

        try:
            response = requests.get(url, headers=headers, params=params)
            if response.status_code == 200:
                items = response.json()['_items']
                if items:
                    user = items[0]['user']
                    g.username = " ".join((user['firstname'],
                                           user['lastname']))
                    return True
                else:
                    raise RuntimeError('You have been logged out because your '
                                       'session is invalid or has expired.')
            else:
                raise RuntimeError('There is an error with the AMIV API, '
                                   'please try again and contact '
                                   'AMIV IT if the problem persists.')
        except (requests.ConnectionError, requests.Timeout):
            raise RuntimeError('The AMIV API is unreachable, '
                               'please try again in a moment and contact AMIV '
                               'IT if the problem persists.')

    # No token provided
    return False
