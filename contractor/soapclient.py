# -*- coding: utf-8 -*-

"""Provide a connector to the AMIV sugarcrm."""

import re
from os import path, makedirs
from io import BytesIO

from PIL import Image
from flask import current_app
from wsgiref.handlers import format_date_time
import requests

from amivcrm import AMIVCRM

from .choices import BoothChoice, PacketChoice


def conf(key):
    """Config Helper to avoid typing a lot."""
    return current_app.config[key]


# Fields needed
FIELDS = [
    # Company info
    'id',
    'name',
    'website',
    'assigned_user_name',
    'employees_ch11_c',
    'employees_world11_c',

    # Address
    'shipping_address_street',
    'shipping_address_street_2',
    'shipping_address_street_3',
    'shipping_address_street_4',
    'shipping_address_city',
    'shipping_address_state',
    'shipping_address_postalcode',
    'shipping_address_country',

    # Fair Signup
    'tag1_c',
    'tag2_c',
    'tischgroesse_c',
    'packet_c',
    'mediapaket_c',
    'kategorie_c',
    'kontaktinfo_c',
    'standplatz11_c',
    'interest_subject11_c',
    'study_contact11_c',
    'job_offer11_c',
    'about_us11_c',
    'our_industries11_c',

]


class Importer(AMIVCRM):
    """Wrapper around CRM class to provide some data parsing."""
    def get_company(self, company_id):
        """Get data for a single company by id."""
        response = self.getentry("Accounts", company_id, select_fields=FIELDS)

        return _parse_response(response)

    def get_companies(self):
        """Get the data from soap to fill in the template.

        Returns:
            tuple (list, dict): The list contains all succesfully imported
                company data.
                The dictionary contains all errors and has the schema:
                companyname: reason for error.
        """
        response = self.get("Accounts",
                            query="accounts_cstm.messeteilnahme_c = 1",
                            order_by="accounts.name",
                            select_fields=FIELDS)

        # Parse data to fit template and collect errors
        data = []
        errors = {}

        for company in response:
            try:
                parsed = _parse_response(company)
            except Exception as err:
                errors[company['name']] = str(err)
            else:
                data.append(parsed)

        return (data, errors)


def _parse_response(response):
    """Parse data for the template.

    Args:
        response (dict): soap response as a dict

    Returns:
        dict: Data formatted for contract creation.
    """
    # Because of the way the amiv letter works, street and city must not be
    # None
    missing = [field for field in ['shipping_address_street',
                                   'shipping_address_postalcode',
                                   'shipping_address_city']
               if response[field] is None]

    if missing:
        raise Exception("The following fields are missing "
                        "for %s: " % response['name'] +
                        ', '.join(missing))

    parsed = {
        'id': response['id'],

        'booth': str(response['standplatz11_c']),
        # For sorting
        'booth_index': _compute_booth_index(response['standplatz11_c']),

        'interested_in': _parse_list(response['interest_subject11_c']),

        'name': response['name'],
        'website': response['website'],

        'contact': response['study_contact11_c'],

        'offers': _parse_offering(response['job_offer11_c']),

        'about': (response['about_us11_c'] or '').lstrip(),
        'focus': (response['our_industries11_c'] or '').lstrip(),

        'first_day': (response['tag1_c'] == '1'),
        'second_day': (response['tag2_c'] == '1'),

        # Info Letter
        'amivrepresentative': response['assigned_user_name'],

        # Full address as string (for fairguide)
        'address': _get_company_address(response),

        # Single address fields (for letter)
        'companyaddress': response['shipping_address_street'],
        'companycity': ("%s %s" % (response['shipping_address_postalcode'],
                                   response['shipping_address_city'])),
    }
    country = response.get('shipping_address_country')
    parsed['companycountry'] = (country if country not in ["Schweiz", None]
                                else "")

    # Packets
    parsed['media'] = (
        PacketChoice.media
        if response['mediapaket_c'] == "mediaPaket" else None)

    parsed['business'] = (
        PacketChoice.business
        if response['packet_c'] == "business" else None)

    parsed['first'] = (
        PacketChoice.first
        if response['packet_c'] == "first" else None)

    # Employees
    def _parse_employees(key, suffix):
        num = re.sub("\D", "", str(response[key]))  # Only numbers
        # Add comma separators for thousands
        if num:
            return "{:,} {}".format(int(num), suffix)

    swiss = _parse_employees('employees_ch11_c',
                             'Schweiz\\minilangsep Switzerland')
    world = _parse_employees('employees_world11_c',
                             'weltweit\\minilangsep worldwide')
    parsed['employees'] = r'\\ '.join(filter(None, [swiss, world]))

    # Get fair days
    parsed['days'] = ''  #default to avoid errors if nothing is selected
    if (response['tag1_c'] == '1') and (response['tag2_c'] == '1'):
        # Both days
        n_days = 2
        parsed['days'] = "both"
        parsed['first_day'] = True
        parsed['second_day'] = True
    else:
        # only one day
        n_days = 1

        # Check which day to print correct date on contract
        if response['tag1_c'] == '1':
            parsed['days'] = "first"
            parsed['first_day'] = True
        if response['tag2_c'] == '1':
            parsed['days'] = "second"
            parsed['second_day'] = True

    # Get booth choice
    # The field 'tischgroesse_c' is weirdly formatted.
    # 'kein' == small both
    # 'ein' == big booth
    # 'zwei' == startup
    if response['tischgroesse_c'] == 'kein':
        boothsize = "startup"
    elif response['tischgroesse_c'] == 'ein':
        boothsize = "small"
    elif response['tischgroesse_c'] == 'zwei':
        boothsize = "big"
    else:
        raise ValueError("Unrecognized value for 'tischgroesse_c': " +
                         response['tischgroesse_c'])

    # Get kategory, map katA to A etc
    mapping = {"kat" + letter: letter for letter in ['A', 'B', 'C']}
    category = mapping[response['kategorie_c']]

    # With size, category and days we can get the right choice
    parsed['boothchoice'] = BoothChoice((boothsize, category, n_days))

    # Get person that did the fair signup:
    # Always at the beginning of the "kontaktinfo_c" field.
    # TODO (Alex): This seems a little hacked to me. As soon as we have a
    #   proper way to identify the main contact we should switch
    try:
        name = ''.join(response['kontaktinfo_c'].split(',')[0:2])
    except Exception:
        raise ValueError("Company representative could not be imported "
                         "from field 'kontaktinfo_c' with content "
                         "'%s'" % response.get('kontaktinfo_c', ''))

    parsed['companyrepresentative'] = name

    # For Logos and Ads, we have to check the kontakt webserver.
    # They are stored with the company name (spaces replaced with _)
    key = parsed['name'].replace(' ', '_')

    # Try to retrieve Logo (path to placeholder logo if not found)
    parsed['logo'] = (_download(conf('LOGO_URL'), key, 'png') or
                      conf('LOGO_MISSING'))

    # If media, try to retrieve advertisement as well
    if parsed['media']:
        parsed['ad'] = (_download(conf('AD_URL'), key, 'pdf') or
                        conf('AD_MISSING'))
    else:
        # Insert placeholder promoting an ad
        parsed['ad'] = conf('AD_AD')

    # Check if any text is provided (and the company can be included in the
    # fair guide)
    # def include_in_fairguide(company):
    parsed['fairguide_ready'] = any(parsed.get(field)
                                    for field in ('contact', 'focus', 'about'))

    return parsed


# Helpers to get Logos


def _process(logo):
    """Remove transparencies because they sometimes cause problems.

    Also re-scale the image if it is more then 2048px wide or high, since
    Latex is not very good at this and produces ugly results.
    """
    converted = logo.convert('RGBA')

    # Rescale if needed
    max_size = conf('MAX_LOGO_DIMENSION')
    size = converted.size

    def _keep_ratio(a, b):
        return int(round((max_size/float(a)) * b))

    if max(size) > max_size:
        if size[0] > size[1]:
            new_width = max_size
            new_height = _keep_ratio(size[0], size[1])
        else:
            new_height = max_size
            new_width = _keep_ratio(size[1], size[0])

        converted = converted.resize((new_width, new_height))

    # Remove Transparencies
    white_back = Image.new('RGB', converted.size, (255, 255, 255))
    white_back.paste(converted, mask=converted.split()[-1])
    return white_back


def _download(base_url, company_name, extension):
    """Download Logo or Ad.

    PNGs are converted and saved with pillow to ensure
    that all metadata is present, since TeX relies on this.
    (And sometimes the files on the server don't include it)

    PDFS are directly saved to disk.
    """
    assert extension in ('png', 'pdf')

    dir = current_app.config['STORAGE_DIR']
    prefix = 'logo' if extension == 'png' else 'ad'
    # Dots in the wrong places can confuse latex includegraphics, remove all
    safe_name = company_name.replace('.', '')
    filepath = path.join(dir, '%s_%s.%s' % (prefix, safe_name, extension))
    url = '%s/%s.%s' % (base_url, company_name, extension)

    # If file already exists, send 'If-Modified-Since' header to only
    # re-download if it has changed
    try:
        timestamp = format_date_time(path.getmtime(filepath))
        headers = {'If-Modified-Since': timestamp}
    except FileNotFoundError:
        headers = {}

    response = requests.get(url, stream=True, headers=headers)
    if response.status_code == 304:  # Not modified on server, nothing to do
        return filepath
    elif response.status_code == 200:
        makedirs(dir, exist_ok=True)

        if extension == 'png':
            logo = Image.open(BytesIO(response.content))
            # Preprocessing for latex and saving
            _process(logo).save(filepath)
        else:
            with open(filepath, 'wb') as file:
                for chunk in response:
                    file.write(chunk)

        return filepath


# Helpers to parse CRM fields

def _parse_list(raw):
    """CRM Format is ^something^,^otherthing^,.."""
    if raw is None:
        return ""
    return [item[1:-1] for item in raw.split(",")]


def _parse_offering(raw):
    """Split offers in three blocks (fulltime, entry, thesis)"""
    # Remove the 'arbeiten' part from Semesterarbeiten etc.
    all_offers = _parse_list(raw)

    offers = {}
    if 'Festanstellungen' in all_offers:
        offers['fulltime'] = 'Festanstellungen'

    entry_offers = [item for item in all_offers
                    if item in ['Praktika', 'TraineeProgramm']]
    if entry_offers:
        offers['entry'] = '\n'.join(entry_offers)

    thesis_offers = list(
        item for item in ['Semester', 'Bachelor', 'Master']
        if "%sarbeiten" % item in all_offers
    )
    # If more then one item, put last on newline
    if len(thesis_offers) > 1:
        thesis_offers[-1] = "\n%s" % thesis_offers[-1]

    # Join them with kommas and write 'arbeiten' after the last one
    if thesis_offers:
        offers['thesis'] = "%sarbeiten" % ', '.join(thesis_offers)

    return offers


def _compute_booth_index(booth_field):
    """Turn the booth into a sortable index, s.t. A12 < A2 etc."""
    if booth_field is None:
        return 0

    first_booth = booth_field.split('/')[0]  # second booth not needed for sort
    numbers = first_booth.replace('A', '0').replace('B', '100')  # ensure A < B
    return int(numbers)


def _get_company_address(result):
    """Build together the adress block."""
    street = result['shipping_address_street']
    city = result['shipping_address_city']
    postal_code = result['shipping_address_postalcode']
    country = result.get('shipping_address_country')
    if country in ["Schweiz", None]:
        country = ""

    return "%s\n%s %s\n%s" % (street, postal_code, city, country)
