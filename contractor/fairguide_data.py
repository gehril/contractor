"""Fairguide data. Eventuelly should be in CRM. Until then here."""

from os import path
from textwrap import dedent


def _image(image):
    basepath = path.dirname(path.abspath(__file__))
    return path.join(basepath, 'images', image)


FAIRGUIDE_DATA = {
    'title': 'Kontakt.18',

    'copies': 800,  # "Auflage"

    # Images
    'title_image': _image('title.jpg'),
    'back_image':  _image('back.jpg'),
    'filler_base': _image('filler'),
    'filler_number': 5,

    # Notes at the end (and additional note page is inserted if the specified
    # number would result in and odd page number)
    'note_page_number': 3,

    # 'back_image'
    'tuesday_left': _image('tuesday_left.jpg'),
    'tuesday_right': _image('tuesday_right.jpg'),
    'wednesday_left': _image('wednesday_left.jpg'),
    'wednesday_right': _image('wednesday_right.jpg'),

    # Fair days (solve this with system locales maybe)
    #TODOOOO
    'first_day': '16',
    'second_day': '17',
    'first_weekday_de': 'Dienstag',
    'first_weekday_en': 'Tuesday',
    'second_weekday_de': 'Mittwoch',
    'second_weekday_en': 'Wednesday',
    'month': '10',
    'year': '2018',
    'start': '11:00',
    'end': '17:00',

    'president': 'Marie Matos',
    'president_welcome_de': dedent("""
        Willkommen zur AMIV Kontakt.18!
        \medbreak
        Die Kontakt ist die Karrieremesse der Studentenvereine AMIV und OBIS,
        die von Studierenden für Studierende organisiert wird. Schon seit 17
        Jahren bietet die Kontakt ETH-Studenten die Möglichkeit,
        Firmenvertreter zu treffen.

        AMIV und OBIS bringen Studierende aus den Fachbereichen Maschinenbau
        und Verfahrenstechnik (MAVT), Informationstechnologie und
        Elektrotechnik (ITET) sowie Management, Technology and Economics (MTEC)
        zusammen.

        Die Studierenden haben die Möglichkeit, mit mehr als 50 Firmen zu
        sprechen, von einem einfachen Gespräch, um einen Überblick über die
        Möglichkeit nach dem Studium zu bekommen, bis zu einer konkreten
        Möglichkeit, sich für eine Masterarbeit, ein Praktikum oder einen Job
        zu bewerben. Seit 2001 hilft die Kontakt Studierenden, ihre Karrieren
        mit Hilfe von Profis zu orientieren.

        Seit dem Aufbau des Teams und unserem ersten Treffen im Februar hat das
        Organisationskomitee hart daran gearbeitet, sowohl Studierenden als
        auch Firmen die bestmögliche Messe zu bieten. Dank zwei zusätzlichen
        Ständen in diesem Jahr können wir zum ersten Mal eine Präsenz von 57
        Firmen verzeichnen.

        Es war meine erste Erfahrung im AMIV und ich hatte die Ehre als
        Präsidentin zu agieren. Ich kann sagen, dass es eine wirklich spannende
        und bereichernde Erfahrung ist.

        Ich möchte dem ganzen Kontakt.18 Organisationskomitee und allen Helfern
        für die grossartige Arbeit danken.

        Ich hoffe, Sie werden die Messe so sehr geniessen, wie ich es genossen
        habe, sie zu organisieren.
        """),
    'president_welcome_en': dedent("""
        Welcome to Kontakt.18!
        \medbreak

        Kontakt is the career fair of the AMIV and OBIS student associations,
        organised by students for students. For 17 years the Kontakt fair
        offers ETH students the possibility to meet companies related to their
        field of study.

        AMIV and OBIS bring together students from the departments of
        Mechanical and Process Engineering (MAVT), Information Technology and
        Electrical Engineering (ITET) and Management, Technology and Economics
        (MTEC).

        Students will have the opportunity to talk to representatives of more
        than 50 companies, from a contact allowing an overview of the
        opportunities after the end of their studies, to the possibility of
        applying for a Master’s thesis, an internship or a job. Since 2001, the
        Kontakt helps students to orient their studies or career paths with
        the help of professionals.

        Starting in February, with the setting up of this year’s team and
        since our first meeting, the organisation committee has worked hard to
        offer students as well as companies an event which we truly hope will
        be an extraordinary experience. This year, we managed to provide two
        additional booths that allow us to welcome 57 companies over the two
        days of the fair  for the first time.

        It was my first participation in the AMIV association, and I had the
        honour to act as the president of the Kontakt commission. It turns out
        to be a great experience and I want to thank all the Kontakt.18
        committee members for their amazing work and everybody who helped us to
        set up this promising event.

        I hope you will enjoy it as much as I enjoyed organising it.
        """),
    'president_image': _image('president.jpg'),

    #TODO
    'rector': 'Sarah Springman',
    'rector_welcome_de': dedent("""
        Liebe Studierende,
        \medbreak
        Sie stehen kurz vor Ihrem Studienabschluss
        und unmittelbar vor dem Übertritt ins Berufsleben.
        Eine attraktive, erfüllende und inspirierende
        Tätigkeit zu finden, ist nicht ganz einfach,
        auch nicht, wenn Ihnen die Welt zu Füssen zu
        liegen scheint. Seien Sie offen, auch für
        Unerwartetes, trauen Sie sich viel zu und
        bleiben Sie vor allem sich selber!
        Wägen Sie ab, reden Sie mit Kolleginnen und
        Kollegen, mit Personen aus Ihrem Bekanntenkreis,
        die schon im Berufsleben stehen. Eine
        wertvolle Gelegenheit, um mit potentiellen
        Arbeitgebern schon heute ins Gespräch zu
        kommen, ist die \emph{AMIV Kontakt.18}. Diese
        Kontaktmesse bietet Ihnen nicht nur eine
        hervorragende Gelegenheit, eine grosse Zahl
        renommierter Arbeitgeber kennenzulernen,
        sondern auch von den Erfahrungen jener
        Personen zu lernen, die den Wechsel von der
        Hochschule ins Berufsleben bereits vollzogen
        haben.

        Nutzen Sie die \emph{AMIV Kontakt.18}, um zu
        erfahren, welche Tätigkeitsfelder Ihnen die
        anwesenden Firmen eröffnen und was Sie in
        Ihrem künftigen beruflichen Umfeld erwarten
        dürfen und worauf Sie bei Ihrer Bewerbung
        achten müssen.

        Fragen Sie, wie Ihr Gesprächspartner oder Ihre
        Gesprächspartnerin beim Einstieg ins Berufsleben
        vorgegangen ist und welche Erfahrungen
        sie oder er dabei gemacht hat. Ich bin sicher,
        dass sich daraus interessante Begegnungen
        und Gespräche ergeben werden.

        Dem Kontakt.18-Team und den involvierten
        Fachvereinen AMIV und OBIS danke ich
        herzlich für die Organisation und das viel
        versprechende Programm.

        Ich wünsche Ihnen viel Erfolg, Freude und
        Erfüllung in Ihrer beruflichen Zukunft.
        """),
    'rector_welcome_en': dedent("""
        Dear students,
        \medbreak
        You are about to take your degrees and begin
        your professional lives. It is a wonderful
        opportunity to start to make your mark on the
        world, with your newly minted qualification as an
        engineer from ETH!
        Your degree opens up so many potential
        careers that it is sometimes a great challenge
        to be able to decide where to begin. There is so
        much choice, so it is important to speak with
        potential employers, and their recent recruits,
        and find out about their company.

        The \emph{AMIV Kontakt.18} event is not only an
        excellent opportunity to learn about a large
        number of well-known employers, but it also
        provides many chances to hear about the
        experience of those who have already made the
        transition from university to career.

        Visit AMIV Kontakt.18 to discover what fields
        the companies operate in, what you might
        expect in your future working environment, and
        what you should pay attention to when you
        apply. Ask your conversation partners about
        their entry into professional life, and the
        experiences that have remained with them. I
        am certain that you will enjoy many interesting
        encounters and discussions.

        I would like to show heartfelt appreciation to
        the Kontakt.18 team and the student associations,
        AMIV and OBIS, for organising this event.
        You have made the first step towards a
        successful career, post ETH, by volunteering to
        help others. Congratulations and thank you!

        I wish you all much success, joy and fulfilent in
        your future careers.
        """),
    'rector_image': _image('rector.jpg'),

    'booth_layout': _image('booth_layout.png'),
    # TODO Booth list

    'support_program': [
        ('Wage Bargaining and Salaries',
         '8.10.2018', '17:15', 'ML E 12', dedent("""
            \medbreak
            The salary is a critical and much-analysed part of any job offer.
            But how high is a typical salary for engineers really?
            Swiss Engineering and the ETH Career Center present information on
            salaries, wage bargaining, and all related aspects to help you
            navigate the process and provide you with an idea for what to
            expect in you next job interview.
            """)),
        ('Wie wegweisend ist die Wahl meines Studiums ' +
         'für meinen Berufseinstieg und meine Karriere?',
         '9.10.2017', '17:15', 'HG F 3', dedent("""
            \medbreak
            \emph{Diese Veranstaltung findet in deutscher Sprache statt.}
            \medbreak
            In Hinblick auf die Karriere nach der ETH kommen viele Fragen
            über das Studium auf, so etwa:
            \\begin{itemize}
            \item Wie wegweisend ist die Wahl des Studiums für den
            Berufseinstieg und die berufliche Laufbahn?
            \item Habe ich mir mit meiner Studienwahl gewisse Karrierewege
            vielleicht schon verbaut?
            \item Wie wegweisend ist das Thema der Masterarbeit für den
            Job-Einstieg?
            \end{itemize}
            Über diese und viele weitere spannende Fragen diskutieren
            VertreterInnen von Axpo, Elca, McKinsey, Tiefbauamt Zürich und
            Zühlke.
            \medbreak
            Anschliessend Apéro mit allen ReferentInnen und weiteren
            Firmenvertretenden.
            """)),
        ('How to create your CV',
         '10.10.2017', '17:15', 'ML E 12', dedent("""
            \medbreak
            The CV is a crucial part of every application. Many people may have
            been surprised by how much time it takes to prioritize and list
            their most important educational milestones and professional
            experiences. During this presentation the ETH Career Center offers
            guidance in creating your CV and answering the most important
            questions about it.
            """)),
        ('Company contact and your personal LinkedIn profile',
         '11.10.2017', '17:15', 'ML F 36', dedent("""
            \medbreak
            Attending career fairs is an essential part of the application
            process. It helps you expand your professional network and get a
            first-hand impression of the companies, their employees and your
            fit into their working environment. In addition to direct contact
            at career fairs, companies are also relying increasingly on social
            media for recruiting and character assessment. We will walk you
            through the most important steps to creating a convincing LinkedIn
            profile and using it to kick-start your career development.
            """)),
        ('What is consulting all about?',
         '12.10.2017', '12:00', 'ML F 34', dedent("""
            \medbreak
            What are the first steps towards becoming a consultant? What does a
            day in the professional life of a consultant look like? Why are
            consulting companies interested in hiring engineering graduates?
            These and many other questions will be addressed at a panel
            discussion with representatives from Bain \& Company, Helbling
            Business Advisors and AWK Group . Use this opportunity to glimpse
            behind the scenes of these consulting companies and learn about
            their daily tasks and challenges.
            \medbreak
            The Apéro following the discussion will further give you an
            excellent chance to speak with the representatives and get your
            individual questions answered.
            """))
    ],

    'cv_check_room': 'LEE E 308',
    'cv_check_intro_de': dedent(r"""
        Während der Messe sind mehrere Consultants
        der Consult \& Pepper AG anwesend
        und helfen, deinen CV auf Hochglanz
        zu polieren. Bring dazu einfach deinen
        ausgedruckten Lebenslauf mit.
        """),
    'cv_check_description_de': dedent("""
        Bei jeder Berbung ist der CV deine Visitenkarte.
        Er sollte auf alle Fragen zu deinen Qualifikationen, Erfahrungen
        und Kenntnissen beantworten und trotzdem kurz und prägnant sein.
        Häufig entscheidet eine erste Durchsicht der Bewerbungsdokumente
        durch den Arbeitgeber, ob die Kandidatin oder der Kandidat auf die
        zu besetzende Stelle passt oder nicht.
        Umso wichtiger ist es, dass dein CV ein professionelles
        Erscheinungsbild hat.

        Dabei helfen dir die Berater unseres Partners und unterstützen
        dich mit ihrer langjährigen Erfahrung.
        """),
    'cv_foto_de': dedent("""
        Zu jedem CV gehört auch ein professionelles
        Bewerbungsfoto. Mit deinem Profilbild
        hinterlässt du den ersten Eindruck, dein
        CV oder Motivationsschreiben überhaupt
        gelesen wird. Umso wichtiger ist es, dabei
        ein ansprechendes Foto zu haben, welches
        den Betrachter überzeugt.
        Deshalb bieten wir in diesm Jahr in Zusammenarbeit
        mit der Consult \& Pepper AG ein kostenloses Fotoshooting an.
        Eine Berufsfotografin rückt dich ins richtige
        Licht und wird dich von deiner besten
        Seite präsentieren.
        """),
    'cv_check_intro_en': dedent(r"""
        During the job fair, several consultants of our partner,
        the Consult \& Pepper AG, are available at your convenience
        and help to brush up your CV. Just bring a copy of it.
        """),
    'cv_check_description_en': dedent("""
        Your CV is the first impression an employer gets of you.
        It should show your qualifications, experiences and knowledge
        and still be comprehensive and concise.
        Oftentimes, the decision, if a candidate is invited for the
        interview, is made after an initial review of all the received
        CVs. It's alll the more important that your CV has a professional
        appearance.

        The consultants of our partner support you with their experience
        of many years to stand out.
        """),
    'cv_foto_en': dedent("""
        Every CV requires a professional picture. With your profile,
        you leave the first impression before your CV or personal statement
        is read. This makes it all the more important to have an
        appealing picture to capture the viewer.

        Therefore, we offer a free fotoshooting with our partner, the
        Consult \& Pepper AG. A professional photographer will
        place you in the right light.
        """),

    # Pages for cv check company and career center
    'cv_check_company_id': '1ff23eb4-c24b-bf2f-e5da-5b855ec0b15e',
    'career_center_id': 'ddb57846-c4ce-599e-a1f0-4d82934bc3ac',


    # Team
    'team_image': _image('team.jpg'),
    'kontakt_team': [
        ('Marie Matos',
         _image('marie.jpg'),
         r'Präsidentin\minilangsep President'),
        ('Patrick Wintermeyer',
         _image('patrick.jpg'),
         r'Quästor\minilangsep Treasurer'),
        ('Silvio Geel',
         _image('silvio.jpg'),
         r'Infrastruktur\minilangsep\\Infrastructure'),
        ('Nicolai Solenthaler',
         _image('nicolai.jpg'),
         r'Rahmenprogramm\minilangsep\\Supporting Program'),
        ('Julia Zahner',
         _image('julia.jpg'),
         r'Catering'),
        ('Luzian Bieri',
         _image('luzian.jpg'),
         r'PR'),
        ('Shuaixin Qi',
         _image('qi.jpg'),
         r'PR'),
        ('Lukas Falcke',
         _image('lukas.jpg'),
         r'OBIS'),
        ('Sandro Lutz', '',
         r'IT'),
        ('Alexander Dietmüller', '',
         r'IT \& Design'),
    ],

    'er_team': [
        'Caroline Dick',
        'Cyriak Heierli',
        'Eike Lage',
        'Gian-Andrea Heinrich',
        'Janko Uehlinger',
        'Julia Jäggi',
        'Leon Hinderling',
        'Niklaus Leuenberger',
        'Pascal Liebherr',
        'Shuaixin Qi',
        'Silvio Geel',
        'Simon Schaefer',
        r'Théophile Messin-\\Roizard',  # Manually break bc. name too long
        'Yves Locher',
    ],

    # Additional Data for info letter only
    'supervisor': 'Marie Matos',
    'supervisor_phone': '+41 78 625 24 72',

    'coffee_room': 'CLA D 19',
    'wifi_name': 'public',
    'wifi_user': 'kontakt.18',
    'wifi_password': 'AmivJobFair18',

    'map': _image('map.png'),

    'setup_start': '8:30',
    'setup_end': '11:00',
    'teardown_start': '17:00',
    'teardown_end': '18:00',
    'apero_start': '17:15',
    'apero_end': '19:30',

    # Data for CV Check Contract
    'cv_check_price': 1000,

}
