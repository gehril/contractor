#-*- coding: utf-8 -*-

"""App settings."""

from os import path
from datetime import datetime as dt

# URL for amivapi
AMIVAPI_URL = 'https://api-dev.amiv.ethz.ch/'

# Development OAuth Id and redirect to default port of flask dev server
OAUTH_ID = 'Local Tool'
OAUTH_URI = 'http://localhost:5000'

# Yearly fair settings, move to CRM as soon as possible
YEARLY_SETTINGS = {
    'fairtitle': 'AMIV Kontakt.19',
    # president of kontakt team
    'president': 'Lina Gehri',
    # Sender of Contracts, usually treasurer of kontakt team
    'sender': 'Luzian Bieri\nKontakt treasurer',

    # Fair days,
    'days': {
        'first': dt(2019, 10, 15),
        'second': dt(2019, 10, 16),
    },

    # Prices, all in francs
    'prices': {
        # Small
        'sA1': '1150',
        'sA2': '2500',
        'sB1': '850',
        'sB2': '1900',
        'sC1': '550',
        'sC2': '1300',
        # Big
        'bA1': '2400',
        'bA2': '5000',
        'bB1': '1800',
        'bB2': '3800',
        # Startups C und B
        'suC1': '250',
        'suC2': '1000',
        'susB1': '550',
        'susB2': '1600',
        # Pakets
        'media': '850',
        'business': '1500',
        'premium': '2500',
        'mediaPlus': '1500',
    },
}

# Download URLs for logos
LOGO_URL = 'http://www.kontakt.amiv.ethz.ch/data/company-logos/2016'
AD_URL = 'http://www.kontakt.amiv.ethz.ch/data/company-inserat'

# Paths to placeholder logo/ad
BASEPATH = path.join(path.dirname(path.abspath(__file__)), 'tex_templates')
LOGO_MISSING = path.join(BASEPATH, 'logo_missing.png')
AD_MISSING = path.join(BASEPATH, 'ad_missing.png')
AD_AD = path.join(BASEPATH, 'ad_ad.png')

# Maximum Dimensions for Logo
MAX_LOGO_DIMENSION = 2048

# Command to compress pdf (here with ghostscripts ebook preset)
COMPRESS_CMD = ("gs -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH "
                "-sOutputFile={output} {input}")