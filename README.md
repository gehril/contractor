# Contractor

Simple tool to create amiv job fair contracts and the fair booklet
from CRM data.

The interface itself is based mainly on [flask](flask.pocoo.org) and uses 
[bootstrap v4](v4-alpha.getbootstrap.com)

## Configuration

A config file with the SugarCRM
credentials is needed. Both username and password can be found in the
[AMIV Wiki](https://intern.amiv.ethz.ch/wiki/SugarCRM#SOAP).

If you are using the production AMIV API, you need to register an OAuth ID
first.
[(More Info)](https://github.com/amiv-eth/amivapi/blob/master/docs/OAuth.md)

Create the file, e.g. called `config.py`:

```python
# Signed sessions
SECRET_KEY = 'Generate something random!'

# CRM connection
SOAP_USERNAME = "..."
SOAP_PASSWORD = "..."

# DINPro Font url (only for docker container)
# FONT_URL = "..."

# AMIVAPI & OAuth (only for production)
# AMIVAPI_URL = 'https://api.amiv.ethz.ch/'
# OAUTH_ID = 'Your Oauth ID'
# OAUTH_URI = 'https://your.url.ch'
```


## Deployment with Docker

A docker image is available under `notspecial/contractor` in the docker hub.
Both the config file and a URL to access the (non-public) DINPro
fonts are needed to run it. The URL can also be found in the
[AMIV Wiki](https://wiki.amiv.ethz.ch/Corporate_Design#DINPro).
Add the url to the config file as shown above.

The most convenient way to pass the config to the container is
using [Docker configs](https://docs.docker.com/engine/swarm/configs).

Set the target of the config to `/contractor/config.py` or set the
environment variable `CONTRACTOR_CONFIG` to the filename of your config
inside of the container.

## Development

For compilation of the tex files,
[amivtex](https://github.com/NotSpecial/amivtex) needs to be installed along
with the DINPro fonts. Take a look at the repository for instructions.

You need Python 3. The following commands create a virtual environment and
install dependencies:

```bash
# (If not yet done: clone and go into folder)
git clone https://github.com/NotSpecial/contractor
cd contractor

# Create and activate virtual environment
python -m venv env
source env/bin/activate

# Install dependencies
pip install -r requirements.txt

# Point app at config (optional, default is 'config.py' in current working dir)
export CONTRACTOR_CONFIG=...

# Start development server
export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run
```

## Testing

There are some tests implemented, especially for tex creation and soap
connection. Use `py.test` to run them.

```
> pip install pytest
> py.test
```

The tests musst be run from the root directory (where the amivtex dir is) or 
the tex tests won't be able to find the .tex source.

*Beware:* The tex test tries a **lot** of choices and takes a lot of time to
finish!
